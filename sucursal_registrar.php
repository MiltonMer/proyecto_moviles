<!– PARA EJEMPLO DASC — >

<?php
$id_sucursal_seleccionada = $_GET['sucursal_id'];
$nombre_sucursal_seleccionada = $_GET['sucursal_nombre'];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php include'inc/incluye_bootstrap.php' ?>


    </head>
    <body>

        <?php include'inc/incluye_menu.php' ?>
      
        <div class="container">
            <div class="jumbotron">
                <form role="form" id="login-form" method="post" class="form-signin" action="refacciones_guardar.php">
                    <div class="h2">
                        Detalles de la sucursal
                    </div>

                    <div class="form-group">
                        <label>ID de la sucursal seleccionada (<?php echo $nombre_marca_seleccionada ?>)</label>
                        <input type="text" id="sucursal_id" class="form-control" name="sucursal_id" value="<?php echo $id_marca_seleccionada ?>" readonly=""
                               placeholder="<?php echo $nombre_sucursal_seleccionada ?>">
                    </div>

                    <div class="form-group">
                        <label>Nombre de la sucursal (requerido)</label>
                        <input type="text" class="form-control" id="nombre_de_sucursal" name="nombre_de_sucursal"
                               placeholder="Ingresa nombre de la sucursal" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Descripci&oacute;n de la sucursal</label>
                        <input type="text" class="form-control" id="descripcion_de_refaccion" name="descripcion_de_sucursal"
                               placeholder="Ingrese descripcion_de_sucursal" style="text-transform:uppercase;">
                    </div>
                    <div class="form-group">
                        <label for="ejemplo_archivo_1">Adjuntar un archivo</label>
                        <input type="file" id="ejemplo_archivo_1">
                        <p class="help-block">No se ha implementado la carga de im&aacute;genes</p>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>
    </body>
</html>
